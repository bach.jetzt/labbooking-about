---
layout: page
title: How To
include_in_header: true
---

# How To
This app is very simple and intuitive in usage but you need to create a team as a starting point.
This is how you start using this app.

<br>

## Open the app
This step is very simple. Just use the web app on https://labbooking.app

#### What can you do?
- Register for an account
- Log into the app
- See an *empty* screen :(
- You will need a **Team** to do more! 

## Request for a `Team`
At this stage of the availability of this app we want to have an idea from what is going on.
That's why we decided to only have manual creation of new teams.
Please write a *short* E-Mail to info@labbooking.app on how you want to use this app:
- Team name
- Company name
- approx number of users
- approx number of rooms
- Your username or email you used in your registration
  + We will configure you as the `Owner` of the new `Team`

## Start inviting `Team Members`
You can invite new members of your team on your own.
Simply use the invite functions within the app.

## Track your room occupations
Go!

